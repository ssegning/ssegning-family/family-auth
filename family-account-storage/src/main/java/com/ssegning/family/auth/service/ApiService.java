package com.ssegning.family.auth.service;

import com.ssegning.family.auth.handler.AccountDataApi;
import com.ssegning.family.auth.handler.AccountsApi;
import com.ssegning.family.auth.handler.CredentialsApi;
import com.ssegning.family.auth.invoker.ApiClient;
import com.ssegning.family.auth.invoker.ApiException;
import com.ssegning.family.auth.model.*;
import lombok.extern.jbosslog.JBossLog;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@JBossLog
public class ApiService {

    private static final String CREDENTIAL_TYPE = PasswordCredentialModel.TYPE.toUpperCase();
    private final AccountsApi accountsApi;
    private final AccountDataApi accountsDataApi;
    private final CredentialsApi credentialsApi;

    public ApiService(@NotNull ApiClient apiClient) {
        this.accountsApi = new AccountsApi(apiClient);
        this.accountsDataApi = new AccountDataApi(apiClient);
        this.credentialsApi = new CredentialsApi(apiClient);
    }

    public ApiAccount addUser(String email) {
        try {
            var input = new ApiCreateAccountInput();
            input.setEmail(email);
            return accountsApi.createAccount(input);
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean deleteAccount(String id) {
        try {
            var externalId = StorageId.externalId(id);
            accountsApi.getAccountByIdentifier(externalId);
            return true;
        } catch (ApiException e) {
            return false;
        }
    }

    public ApiAccount getUserById(String id) throws ApiException {
        var externalId = StorageId.externalId(id);
        return accountsApi.getAccountByIdentifier(externalId);
    }

    public ApiAccount getAccountByEmail(@Email String email) throws ApiException {
        return accountsApi.getAccountByIdentifier(email);
    }

    public boolean isValid(String id, String challenge) {
        try {
            var input = new ApiValidatePasswordInput();
            input.setChallenge(challenge);

            var externalId = StorageId.externalId(id);
            var response = credentialsApi.validateCredential(externalId, CREDENTIAL_TYPE, input);
            return response.getValid() != null && response.getValid();
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean updateCredential(String id, UserCredentialModel input) {
        if (!input.getType().toUpperCase().equals(CREDENTIAL_TYPE)) {
            return false;
        }

        var externalId = StorageId.externalId(id);
        var credentialInput = new ApiCreateCredentialInput();
        credentialInput.setChallenge(input.getChallengeResponse());

        try {
            var response = credentialsApi.createCredential(externalId, CREDENTIAL_TYPE, credentialInput);
            return response.getState() != null && response.getState();
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isConfigured(String id, String type) {
        try {
            var externalId = StorageId.externalId(id);
            var response = credentialsApi.hasCredential(externalId, type.toUpperCase());
            return response.getState() != null && response.getState();
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public void disableCredential(String id, String credentialType) {
        var externalId = StorageId.externalId(id);
        try {
            credentialsApi.disableCredential(externalId, credentialType.toUpperCase());
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public void updatePersonalData(String id, ApiChangePersonalDataRequest request) {
        try {
            accountsDataApi.updatePersonalData(id, request);
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public List<ApiAccount> findAccountByAll(int size, int offset, Map<String, String> map) {
        var request = new ApiFindByAccountMap().map(map);
        try {
            return this.accountsApi.findAccountByAll(BigDecimal.valueOf(size), BigDecimal.valueOf(offset), request);
        } catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

}
