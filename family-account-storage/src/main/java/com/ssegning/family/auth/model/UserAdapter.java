package com.ssegning.family.auth.model;

import com.ssegning.family.auth.service.ApiService;
import lombok.ToString;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang.StringUtils;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.representations.IDToken;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ToString
@JBossLog
public class UserAdapter extends AbstractUserAdapterFederatedStorage {
    private final ApiAccount account;
    private final ApiService apiService;

    public UserAdapter(KeycloakSession session,
                       RealmModel realm,
                       ComponentModel storageProviderModel,
                       @NotNull ApiAccount account,
                       @NotNull ApiService apiService) {
        super(session, realm, storageProviderModel);
        this.account = account;
        this.apiService = apiService;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public void setUsername(String username) {
        setEmail(username);
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        var map = new MultivaluedHashMap<String, String>();
        map.put(IDToken.PHONE_NUMBER, Collections.singletonList(account.getPhoneNumber()));
        map.put(FIRST_NAME, Collections.singletonList(account.getFirstName()));
        map.put(LAST_NAME, Collections.singletonList(account.getLastName()));
        map.put(EMAIL, Collections.singletonList(account.getEmail()));
        map.put(USERNAME, Collections.singletonList(account.getEmail()));

        // map.put(IDToken.LOCALE, Collections.singletonList(account.getLocale()));
        // if (account.getPhoneNumberVerified() != null) {
        //     map.put("phone_number_verified", Collections.singletonList(account.getPhoneNumberVerified().toString()));
        // }

        ApiAccountAvatar avatar = account.getAvatar();
        if (avatar != null) {
            map.put("avatarUrl", Collections.singletonList(avatar.getAvatarUrl()));
        }

        map.put("bio", Collections.singletonList(account.getBio()));
        if (account.getGender() != null) {
            map.put("gender", Collections.singletonList(account.getGender().getValue()));
        } else {
            map.put("gender", Collections.singletonList(null));
        }

        return map;
    }

    @Override
    public String getFirstAttribute(String name) {
        Map<String, List<String>> attributes = getAttributes();
        List<String> values = attributes.get(name);
        if (values != null && values.size() > 0) {
            return values.get(0);
        }
        return null;
    }

    @Override
    public String getId() {
        if (storageId == null) {
            if (account.getId() == null) {
                throw new IllegalArgumentException("Cannot have an account without id");
            }
            storageId = new StorageId(storageProviderModel.getId(), account.getId());
        }
        return storageId.getId();
    }

    @Override
    public boolean isEnabled() {
        if (account.getStatus() != null) {
            return account.getStatus().equals(ApiAccount.StatusEnum.ALLOWED);
        }
        return false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (account.getStatus() != null && account.getStatus().equals(ApiAccount.StatusEnum.ALLOWED)) {
            return;
        }
        log.info(String.format("UserAdapter#setEnabled : Should set enabled to %s", enabled));
        account.setStatus(ApiAccount.StatusEnum.ALLOWED);
    }

    @Override
    public boolean isEmailVerified() {
        return account.getEmailVerified() != null && account.getEmailVerified();
    }

    @Override
    public void setEmailVerified(boolean verified) {
        log.info(String.format("Cannot update this : %s - %s", "emailVerified", verified));
    }

    @Override
    public Set<GroupModel> getGroups() {
        return Set.of();
    }

    @Override
    public long getGroupsCount() {
        return 0;
    }

    @Override
    public long getGroupsCountByNameContaining(String search) {
        return 0;
    }

    @Override
    public Long getCreatedTimestamp() {
        return account.getCreatedAt().longValue();
    }

    @Override
    public void setSingleAttribute(String name, String value) {
        if (!StringUtils.isEmpty(value) && account.getId() != null) {
            var request = buildChangeR(name, value);

            apiService.updatePersonalData(account.getId(), request);

            updateUserAttribute(name, value);
        }
    }

    private ApiChangePersonalDataRequest buildChangeR(String name, String value) {
        var request = new ApiChangePersonalDataRequest();

        switch (name) {
            case "gender":
                ApiChangePersonalDataRequest.GenderEnum gender = ApiChangePersonalDataRequest.GenderEnum.fromValue(value);
                request.setGender(gender);
                break;
            case "phoneNumber":
                request.setPhoneNumber(value);
                break;
            case FIRST_NAME:
                request.setFirstName(value);
                break;
            case LAST_NAME:
                request.setLastName(value);
                break;
            case EMAIL:
                request.setEmail(value);
                break;
            case "bio":
                request.setBio(value);
                break;
            default:
                log.info(String.format("Cannot update this : %s - %s", name, value));
                break;
        }
        return request;
    }

    private void updateUserAttribute(String name, String value) {
        switch (name) {
            case FIRST_NAME:
                account.setFirstName(value);
                break;
            case LAST_NAME:
                account.setLastName(value);
                break;
            case EMAIL:
                account.setEmail(value);
                break;
            case "bio":
                account.setBio(value);
                break;
            case "gender":
                account.setGender(ApiAccount.GenderEnum.fromValue(value));
                break;
            case "phoneNumber":
                account.setPhoneNumber(value);
                break;
        }
    }

    @Override
    public void setAttribute(String name, List<String> values) {
        if (name != null
                && values != null && values.size() > 0
                && !StringUtils.isEmpty(values.get(0))
                && account.getId() != null) {
            var value = values.get(0);

            setSingleAttribute(name, value);
        }
    }

}
