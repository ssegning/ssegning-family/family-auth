package com.ssegning.family.auth.storage;

import com.ssegning.family.auth.invoker.ApiClient;
import com.ssegning.family.auth.invoker.Configuration;
import lombok.NoArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang.StringUtils;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.provider.ServerInfoAwareProviderFactory;
import org.keycloak.storage.UserStorageProviderFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@JBossLog
@NoArgsConstructor
public class MfsStorageProviderFactory
        implements UserStorageProviderFactory<MfsStorageProvider>,
        ServerInfoAwareProviderFactory {

    private static final Map<String, String> infos = new HashMap<>();
    private static final List<ProviderConfigProperty> configProperties;
    private static final String SERVER_URL_KEY = "serverUrl";
    private static final String SERVER_BASIC_USER = "basicUser";
    private static final String SERVER_BASIC_PASS = "basicPass";

    static {
        infos.put("version", "1.2.0");

        configProperties = ProviderConfigurationBuilder
                .create()
                .property()
                .name(SERVER_URL_KEY)
                .type(ProviderConfigProperty.STRING_TYPE)
                .label("Server Url")
                .defaultValue("http://host.docker.internal:3000")
                .helpText("Url where to call APIs")
                .add()
                //
                .property()
                .name(SERVER_BASIC_USER)
                .type(ProviderConfigProperty.STRING_TYPE)
                .label("Basic user")
                .defaultValue("user")
                .helpText("Username for basic auth")
                .add()
                //
                .property()
                .name(SERVER_BASIC_PASS)
                .type(ProviderConfigProperty.PASSWORD)
                .label("Basic password")
                .defaultValue("password")
                .helpText("Password for basic auth")
                .add()
                //
                .build();
    }

    public final String ID = "family-storage-provider";
    private ApiClient apiClient;

    @Override
    public MfsStorageProvider create(KeycloakSession keycloakSession, ComponentModel componentModel) {
        if (apiClient == null) {
            createApiClient(componentModel.getConfig());
        }
        return new MfsStorageProvider(keycloakSession, new Properties(), componentModel, apiClient);
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getHelpText() {
        return "Family Account storage";
    }

    @Override
    public Map<String, String> getOperationalInfo() {
        return infos;
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel model) throws ComponentValidationException {
        MultivaluedHashMap<String, String> config = model.getConfig();

        String serverUrl = config.getFirst(SERVER_URL_KEY);
        if (StringUtils.isEmpty(serverUrl)) {
            throw new ComponentValidationException("ServerUrl should not be empty");
        }
        try {
            new URL(serverUrl);
        } catch (MalformedURLException malformedURLException) {
            throw new ComponentValidationException("ServerUrl is not a valid url");
        }

        String basicUser = config.getFirst(SERVER_BASIC_USER);
        if (StringUtils.isEmpty(basicUser)) {
            throw new ComponentValidationException("Basic User should not be empty");
        }

        String basicPass = config.getFirst(SERVER_BASIC_PASS);
        if (StringUtils.isEmpty(basicPass)) {
            throw new ComponentValidationException("Basic Password should not be empty");
        }
    }

    @Override
    public void onCreate(KeycloakSession session, RealmModel realm, ComponentModel model) {
        createApiClient(model.getConfig());
    }

    @Override
    public void onUpdate(KeycloakSession session, RealmModel realm, ComponentModel oldModel, ComponentModel newModel) {
        createApiClient(newModel.getConfig());
    }

    private void createApiClient(MultivaluedHashMap<String, String> config) {
        apiClient = Configuration.getDefaultApiClient();

        String serverUrl = config.getFirst(SERVER_URL_KEY);
        apiClient.setBasePath(serverUrl);

        String basicUser = config.getFirst(SERVER_BASIC_USER);
        apiClient.setUsername(basicUser);

        String basicPass = config.getFirst(SERVER_BASIC_PASS);
        apiClient.setPassword(basicPass);
    }

}
