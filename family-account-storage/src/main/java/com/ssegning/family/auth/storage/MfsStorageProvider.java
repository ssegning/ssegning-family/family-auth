package com.ssegning.family.auth.storage;

import com.ssegning.family.auth.invoker.ApiClient;
import com.ssegning.family.auth.invoker.ApiException;
import com.ssegning.family.auth.model.ApiAccount;
import com.ssegning.family.auth.model.UserAdapter;
import com.ssegning.family.auth.service.ApiService;
import lombok.extern.jbosslog.JBossLog;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.*;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@JBossLog
public class MfsStorageProvider
        implements UserStorageProvider,
        UserLookupProvider,
        UserQueryProvider,
        CredentialInputValidator,
        CredentialInputUpdater,
        UserRegistrationProvider {

    protected final KeycloakSession session;
    protected final Properties properties;
    protected final ComponentModel model;
    protected final ApiService apiService;

    public MfsStorageProvider(KeycloakSession session,
                              Properties properties,
                              ComponentModel model,
                              @NotNull ApiClient apiClient) {
        this.session = session;
        this.properties = properties;
        this.model = model;
        this.apiService = new ApiService(apiClient);
    }

    @Override
    public void close() {

    }

    @Override
    public UserModel getUserById(String id, RealmModel realm) {
        try {
            String userId = StorageId.externalId(id);
            ApiAccount account = apiService.getUserById(userId);
            return createAdapter(realm, account);
        } catch (ApiException e) {
            return null;
        }
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        return getUserByEmail(username, realm);
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        try {
            ApiAccount account = apiService.getAccountByEmail(email);
            return createAdapter(realm, account);
        } catch (ApiException e) {
            return null;
        }
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        return credentialType.equals(PasswordCredentialModel.TYPE);
    }

    @Override
    public boolean updateCredential(RealmModel realm, UserModel user, CredentialInput input) {
        var credI = getUserCredential(user, input);
        if (credI == null) {
            return false;
        }
        var userId = StorageId.externalId(user.getId());
        return apiService.updateCredential(userId, credI);
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel user, String credentialType) {
        var userId = StorageId.externalId(user.getId());
        apiService.disableCredential(userId, credentialType);
    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realm, UserModel user) {
        return Set.of(PasswordCredentialModel.TYPE);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        var userId = StorageId.externalId(user.getId());
        return apiService.isConfigured(userId, credentialType);
    }

    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput input) {
        var credI = getUserCredential(user, input);
        if (credI == null) {
            return false;
        }

        var userId = StorageId.externalId(user.getId());
        return apiService.isValid(userId, credI.getChallengeResponse());
    }

    private UserCredentialModel getUserCredential(UserModel user, CredentialInput input) {
        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) {
            return null;
        }

        if (input.getChallengeResponse() == null) {
            return null;
        }

        return (UserCredentialModel) input;
    }

    private UserModel createAdapter(RealmModel realm, ApiAccount account) {
        return new UserAdapter(session, realm, model, account, apiService);
    }

    @Override
    public UserModel addUser(RealmModel realm, String username) {
        ApiAccount account = apiService.addUser(username);
        return createAdapter(realm, account);
    }

    @Override
    public boolean removeUser(RealmModel realm, UserModel user) {
        String userId = StorageId.externalId(user.getId());
        return apiService.deleteAccount(userId);
    }

    @Override
    public void preRemove(RealmModel realm) {
    }

    @Override
    public void preRemove(RealmModel realm, GroupModel group) {

    }

    @Override
    public void preRemove(RealmModel realm, RoleModel role) {

    }

    @Override
    public int getUsersCount(RealmModel realm) {
        return getUsers(realm).size();
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm) {
        return getUsers(realm, 0, Integer.MAX_VALUE);
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
        return searchForUser((String) null, realm, firstResult, maxResults);
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm) {
        return searchForUser(search, realm, 0, Integer.MAX_VALUE);
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
        return searchForUser(Map.of("search", search), realm, firstResult, maxResults);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
        return searchForUser(params, realm, 0, Integer.MAX_VALUE);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult, int maxResults) {
        Function<ApiAccount, UserModel> mapper = a -> createAdapter(realm, a);
        return apiService
                .findAccountByAll(firstResult, maxResults, params)
                .stream()
                .map(mapper)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        return Collections.emptyList();
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
        return getGroupMembers(realm, group, 0, Integer.MAX_VALUE);
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
        return searchForUser(Map.of(attrName, attrValue), realm);
    }
}
