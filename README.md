# Family Auth

## Deployment Chart

Update repo
```bash
helm repo update
```

Update dependency
```bash
helm dependency update ./
```

Lint
```bash
helm lint ./ -f ./stages/dev/values.yaml
```

Dry run release
```bash
helm upgrade keycloak ./ -i --dry-run --debug -n parcaune-yallhair-dev -f ./stages/dev/values.yaml
```

Install/Upgrade release
```bash
helm upgrade keycloak ./ -i --debug --wait -n parcaune-yallhair-dev -f ./stages/dev/values.yaml
```

PROD: Install or Upgrade release
```bash
helm upgrade --atomic --cleanup-on-fail --timeout 60m0s -i keycloak ./ -n parcaune-yallhair-dev -f ./stages/dev/values.yaml
```

Delete release
```bash
helm delete keycloak -n family
```

## Other docs

### In this repository
- [Family login theme](family-theme/README.md)
