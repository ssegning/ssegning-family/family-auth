# Family theme 

This dir contains the theme which will be used by the Keycloak
make user login into their applications.

We can see some image of the process here:

1. Login screens:
   
   | Medium to large Screen | Phone-like Screen |
    :-------------------------:|:-------------------------:|
    [![Login page medium](../imgs/kc-login-medium.png)](../imgs/kc-login-medium.png) | [![Login page small](../imgs/kc-login-small.png)](../imgs/kc-login-small.png)

2. Registration screen:

   | Medium to large Screen | Phone-like Screen |
    :-------------------------:|:-------------------------:|
   [![Registration top](../imgs/kc-registration-top.png)](../imgs/kc-registration-top.png) | [![Registration top](../imgs/kc-registration-small-top.png)](../imgs/kc-registration-small-top.png)
   [![Registration top](../imgs/kc-registration-bottom.png)](../imgs/kc-registration-bottom.png) | [![Registration top](../imgs/kc-registration-small-bottom.png)](../imgs/kc-registration-small-bottom.png)


3. Password fogotten:
   [![Registration top](../imgs/kc-password-forgotten.png)](../imgs/kc-password-forgotten.png)
