#################################################################

FROM maven:3.6.3-jdk-11 AS MAVEN_BUILD

WORKDIR /app

COPY . .

RUN mvn clean package

#################################################################

FROM node:14-alpine AS THEME_BUILD

RUN apk update && apk add yarn curl bash python g++ make && rm -rf /var/cache/apk/*

# install node-prune (https://github.com/tj/node-prune)
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

WORKDIR /app

COPY ./family-theme/family-login ./

WORKDIR /app/common/resources

RUN yarn --frozen-lockfile
RUN yarn scss-build
RUN npm prune --production
RUN /usr/local/bin/node-prune
RUN rm -rf package.json yarn.lock .dockerignore .gitignore scss

#################################################################

FROM jboss/keycloak:12.0.1

USER 0

COPY --from=MAVEN_BUILD /app/family-account-storage/target/family-account-storage.jar ${JBOSS_HOME}/providers/family-account-storage.jar
COPY --from=MAVEN_BUILD /app/family-account-storage/target/family-account-storage-jar-with-dependencies.jar ${JBOSS_HOME}/providers/family-account-storage-jar-with-dependencies.jar
COPY --from=THEME_BUILD /app/ ${JBOSS_HOME}/themes/family-login

USER 1000

#################################################################
